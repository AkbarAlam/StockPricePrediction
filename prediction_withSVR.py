import csv
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt

# an empty array to keep ta data which will be processed 
dates = []
prices = []

#reading the data from CSV file
# this process can be done with Pandas as well
def get_data(filename):
    with open(filename, 'r') as csvfile:
        csvFileReader = csv.reader(csvfile)
        next(csvFileReader)  
        for row in csvFileReader:
            dates.append(int(row[0].split('-')[0]))
            prices.append(float(row[1]))
    return

def predict_price(dates, prices, x):
    # converting the dates to matrix of n X 1
    dates = np.reshape(dates, (len(dates), 1))  

    # using different SVR kernels to observe result
    svr_len = SVR(kernel='linear', C=1e3)
    svr_poly = SVR(kernel='poly', C=1e3, degree=2)
    svr_rbf = SVR(kernel='poly', C=1e3, gamma=0.1)

    #fiting the data to the model
    svr_len.fit(dates, prices)
    svr_poly.fit(dates, prices)
    svr_rbf.fit(dates, prices)

    #all data
    plt.scatter(dates, prices, color='black', label='Data')

    # ploting the models 
    plt.plot(dates, svr_len.predict(dates), color='red', label='Linear model')
    plt.plot(dates, svr_poly.predict(dates), color='blue', label='polynomial model')
    plt.plot(dates, svr_rbf.predict(dates), color='green', label='RBF model')

    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Support vector Regression')
    plt.legend()
    plt.show()

    return svr_len.predict(x)[0], svr_poly.predict(x)[0], svr_rbf.predict(x)[0]

# reading data from CSV file by calling the method
get_data('aapl.csv')  
print("Dates- ", dates)
print("Prices- ", prices)

# calling this method will show the prediction
predicted_price = predict_price(dates, prices, 29)  

