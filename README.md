# Project for Advance Modeling and simulation Course

This project is about **predicting stock price**. For this project apple's stock price has been collected from the google finance. The data was arranged as a CSV file.  

Two approaches have been used to read/get the data from the CSV file. One with _Pandas_ and another one is _CSV_.

_Scikit-learn_ has been used to perform the analytics. In this project, two algorithms have been used. 

* Linear Regression.
* Support Vector Regrassion.

The results can be found in the plots folder.