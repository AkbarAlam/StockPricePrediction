import csv
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt
import pandas as pd


df = pd.read_csv('aapl.csv')

dates = df.index.values
price = df['Close'].values

dates = np.reshape(dates,(len(dates), 1)) # converting to matrix of n X 1

svr_lin = SVR(kernel= 'linear', C= 1e3)
svr_poly = SVR(kernel= 'poly', C= 1e3, degree= 2)
svr_rbf = SVR(kernel= 'rbf', C= 1e3, gamma= 0.1) # defining the support vector regression models
svr_rbf.fit(dates, price) # fitting the data points in the models
svr_lin.fit(dates, price)
svr_poly.fit(dates, price)

plt.scatter(dates, price, color= 'black', label= 'Data') # plotting the initial datapoints
plt.plot(dates, svr_rbf.predict(dates), color= 'red', label= 'RBF model') # plotting the line made by the RBF kernel
plt.plot(dates,svr_lin.predict(dates), color= 'green', label= 'Linear model') # plotting the line made by linear kernel
plt.plot(dates,svr_poly.predict(dates), color= 'blue', label= 'Polynomial model') # plotting the line made by polynomial kernel
plt.xlabel('Date')
plt.ylabel('Price')
plt.title('Support Vector Regression')
plt.legend()
plt.show()

